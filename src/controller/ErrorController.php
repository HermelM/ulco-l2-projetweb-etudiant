<?php

namespace controller;

use http\Params;

class ErrorController {

  public function error(): void
  {
    $params=[
        "title"=> "error",
        "module"=>"error.php"
    ];
    \view\Template::render($params);
  }

}