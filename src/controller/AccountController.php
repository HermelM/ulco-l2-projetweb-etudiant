<?php


namespace controller;

use view\Template;

class AccountController
{

    public function account():void{
        $params=[
            "title"=> "account",
            "module"=>"account.php"
        ];
        \view\Template::render($params);
    }

}