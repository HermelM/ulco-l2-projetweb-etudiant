<?php $info = $params["info"]; ?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $info["image"]?>" id="grandeImg">

            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $info["image"]?>">
                </div>
                <div>
                    <img src="/public/images/<?= $info["image1"]?>">
                </div>
                <div>
                    <img src="/public/images/<?= $info["image2"]?>">
                </div>
                <div>
                    <img src="/public/images/<?=  $info["image3"]?>">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category">
                <?= $info["c_name"]?>
            </p>
            <h1>
                <?= $info["name"]?>
            </h1>
            <p class="product-price">
                <?= $info["price"]?>
            </p>
            <form id="form">
                <button type="button" id="button_moins">-</button>
                <button type="button" id="chiffre">1</button>
                <button type="button" id="button_plus">+</button>
                <input type="submit" >
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>
                Spécificités
            </h2>
            <?= $info["spec"]?>
        </div>
        <div class="product-comment">
            <h2>
                Avis
            </h2>
            <p>
                Il n'y as pas d'avis pour ce produit.
            </p>
        </div>
    </div>
</div>

<script src="/public/script/product.js"></script>