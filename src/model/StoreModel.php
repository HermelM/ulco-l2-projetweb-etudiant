<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts():array
  {
      $db=\model\Model::connect();

      $sql="SELECT product.id, product.name,price,product.image, category.name as c_name FROM product INNER JOIN category ON product.category = category.id";

      $req=$db->prepare($sql);
      $req->execute();

      return $req->fetchAll();

  }
    static function infoProduct(int $id)
    {
        $db=\model\Model::connect();

        $sql="SELECT product.name, price,product.image,product.image_alt1 AS image1,product.image_alt2 AS image2,product.image_alt3 AS image3,spec,category.name as c_name FROM product INNER JOIN category WHERE product.id=$id";

        $req=$db->prepare($sql);
        $req->execute();

        return $req->fetch();
    }
}