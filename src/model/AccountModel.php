<?php


namespace model;


class AccountModel
{

     static function check($firstname,$lastname,$mail,$password) : bool{

         $db=\model\Model::connect();

         $sql="SELECT account.mail FROM account";

         $req=$db->prepare($sql);
         $req->execute();

         if (filter_var($mail,FILTER_VALIDATE_EMAIL) &&
             strlen($firstname)>=2 &&
             strlen($lastname)>=2 &&
             strlen($password)>=6 &&
             $mail!=$req->fetchAll()){
             return true;
         }
         else{
             return false;
         }

     }

     static function signin($firstname,$lastname,$mail,$password){

        if(self::check($firstname,$lastname,$mail,$password)){
            $db=\model\Model::connect();

            $sql="INSERT INTO account(firstname,lastname,mail,password) VALUES (?,?,?,?)";

            $req=$db->prepare($sql);
            $req->execute(array($firstname,$lastname,$mail,$password));
            return true;
        }
        return false;
     }

}