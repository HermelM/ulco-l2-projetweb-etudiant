document.addEventListener('DOMContentLoaded',function(){

    let minus=document.getElementById('button_moins')
    let plus=document.getElementById('button_plus')
    let number=document.getElementById('chiffre')
    let div=document.createElement('div')
    let form=document.getElementById('form')
    let miniature=document.querySelector(".product-miniatures")
    let grandeImg= document.getElementById('grandeImg')
    let imgMiniature=miniature.getElementsByTagName('img')


    plus.addEventListener('click',function (){

        if(number.textContent==='4'){
            div.classList.add('box')
            div.classList.add('error')
            div.setAttribute('id','divError')
            div.innerHTML="Quantité maximale autorisée !"
            form.appendChild(div)
        }

        if(number.textContent!=='5'){
            number.innerHTML++
        }

    })

    minus.addEventListener('click',function (){
        if(number.textContent!=1){
            number.innerHTML--
        }
        let divError=document.getElementById('divError')

        if(document.body.contains(divError)){
            form.removeChild(div)
        }
    })

    for(let imgs of imgMiniature){

        imgs.addEventListener('click',function(){
            grandeImg.setAttribute('src',imgs.src)
        })
    }

})

